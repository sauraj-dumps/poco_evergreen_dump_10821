#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_evergreen.mk

COMMON_LUNCH_CHOICES := \
    lineage_evergreen-user \
    lineage_evergreen-userdebug \
    lineage_evergreen-eng
